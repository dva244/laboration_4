#define _CRT_SECURE_NO_WARNINGS // Behovs for vissa funktioner i visual studio
#include "HashTable.h"
#include "Bucket.h"
#include<assert.h>
#include<stdlib.h>
#include<stdio.h>


//=============================================================================
/* returns a hash index */
static int hash(Key key, int tablesize)
{
    return (key % tablesize);
}
//=============================================================================
/* returns the index of an empty spot in the hashtable using linearprobe */
static int linearProbe(const HashTable* htable, Key key, unsigned int *col)
{
    while((*htable).table[hash((key + *col), (*htable).size)].key != UNUSED)
    {
        if(*col == (*htable).size)
        {
            return -1;
        }
        (*col)++;
    }

    return hash((key + *col), (*htable).size);
}
//=============================================================================
/* creates a hashtable by dynamicly allocating memory for a hashtable */
HashTable createHashTable(unsigned int size)
{
    HashTable htable;

    (htable).table = (struct Bucket*)malloc(sizeof(struct Bucket)*size);
    
    //Should check if allocation was succesful before proceeding ((htable).table != NULL)

    (htable).size = size;

    return htable;
}
//=============================================================================
/* inserts an element into the hastable and returns the number of colisions that occured */
unsigned int insertElement(HashTable* htable, const Key key, const Value value)
{
    unsigned int col = 0;
    unsigned int index;
    int i = 0;

    if (lookup(htable, key) != NULL)    //if key is already in the hastable
    {
        while(((*htable).table[i].key != key) && (i < (*htable).size))
        {
            i++;
        }
        (*htable).table[i].value = value;   //updates it's value
    }

    else if ((index = linearProbe(htable, key, &col)) >= 0)
    {
        (*htable).table[index].key = key;
        (*htable).table[index].value = value;
    }

    assert(lookup(htable, key) != NULL);

    return col;
}
//=============================================================================
/* Tar bort datat med nyckel "key" */
void deleteElement(HashTable* htable, const Key key)
{
    int i = 0;
    int deleted;
    int index;
    int tempkey;
    Person tempvalue;

    if(lookup(htable, key) == NULL) //key isn't found in the hashtable
    {
        return;
    }

    while((*htable).table[hash(key + i, (*htable).size)].key != key)
    {
        i++;
    }
    deleted = key + i;
    index = hash(deleted, (*htable).size);
    (*htable).table[index].key = UNUSED;
    (*htable).table[index].value.personalNumber = 0;
    (*htable).table[index].value.weight = 0.0;
    (*htable).table[index].value.name[0] = 0;


    //re adds all the elements after the deleted one
    i = 1;
    while((i < (*htable).size) && ((*htable).table[hash(deleted + i, (*htable).size)].key != UNUSED))
    {
        index = hash(deleted + i, (*htable).size);
        tempkey = (*htable).table[index].key;
        tempvalue = (*htable).table[index].value;

        (*htable).table[index].key = UNUSED;
        (*htable).table[index].value.personalNumber = 0;
        (*htable).table[index].value.weight = 0.0;
        (*htable).table[index].value.name[0] = 0;

        insertElement(htable, tempkey, tempvalue);

        i++;
    }

    assert(lookup(htable, key) == NULL);
}
//=============================================================================
/* Returnerar en pekare till vardet som key ar associerat med eller NULL om ingen sadan nyckel finns */
const Value* lookup(const HashTable* htable, const Key key)
{
    int i = 0;

    while(((*htable).table[hash(key + i, (*htable).size)].key != key) && (i < (*htable).size))
    {
        i++;
    }

    if ((*htable).table[hash(key + i, (*htable).size)].key == key)
    {
        return &(*htable).table[hash(key + i, (*htable).size)].value;
    }

    return NULL;
}
//=============================================================================
/* free the entire hashtable */
void freeHashTable(HashTable* htable)
{
    int i = (*htable).size - 1;
    int index;

    while(i != 0)
    {
        index = hash(i, (*htable).size);
        (*htable).table[index].key = UNUSED;
        (*htable).table[index].value.personalNumber = 0;
        (*htable).table[index].value.weight = 0.0;
        (*htable).table[index].value.name[0] = 0;
        i--;
    }
    (*htable).size = i;
    free((*htable).table);
    (*htable).table = NULL;

    assert((*htable).size == 0);
}
//=============================================================================
/* returns the size of the hashtable */
unsigned int getSize(const HashTable* htable)
{
    return (*htable).size;
}
//=============================================================================
/* prints the hashtable */
void printHashTable(const HashTable* htable)
{
    int i = 0;

    printf("\nHashtable: ");
    while(i < (*htable).size)
    {
        printf("\nKey: %d", (*htable).table[i].key);
        printf("\n");
        printPerson(&(*htable).table[i].value, i);
        printf("\n");
        i++;
    }
}
